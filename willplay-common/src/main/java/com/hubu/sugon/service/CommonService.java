package com.hubu.sugon.service;


import com.hubu.sugon.entity.PageInfoResult;
import com.hubu.sugon.entity.Result;
import com.hubu.sugon.entity.RetCodeEnum;

import java.util.List;

public interface CommonService {
    /**
     * 返回结果集封装
     * @param code
     * @param success
     * @param msg
     * @param data
     * @param <T>
     * @return
     * @throws Exception
     */
    <T> Result initReturnResult(int code, boolean success, String msg, T data) throws Exception;

    /**
     * 返回结果集封装
     * @param retCodeEnum
     * @param data
     * @param success
     * @param msg
     * @param <T>
     * @return
     */
    <T> Result initReturnResult(RetCodeEnum retCodeEnum, T data, boolean success, String msg);

    /**
     * 返回结果集封装
     * @param retCodeEnum
     * @param success
     * @param msg
     * @return
     */
    Result initReturnResult(RetCodeEnum retCodeEnum, boolean success, String msg);

    /**
     * 检查空属性
     * @param fields
     * @return
     */
    boolean checkExistsEmptyFields(Object... fields);

    /**
     * 构建返回结果集，只适用包含一个key的情况
     * @param key
     * @param object
     * @param message
     * @return
     * @throws Exception
     */
    <T> Result returnResult(String key, T object, String message) throws Exception;

    /**
     * 转换object
     * @param value
     * @return
     */
    Integer convertObjectToInteger(Object value);

    /**
     * 分页封装
     */
    public <T> PageInfoResult toPage(List<T> list);
}
