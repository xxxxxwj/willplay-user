package com.hubu.sugon.entity;

import lombok.Data;

import java.util.List;

@Data
public class PageInfoResult<T> {

    private List<T> rows;

    private Integer pageNum;

    private Integer pageSize;

    //@JsonSerialize(using = ToStringSerializer.class)
    private Long total;
}
