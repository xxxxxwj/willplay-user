package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class Result<T> {
    private boolean success = true;

    @ApiModelProperty
    private T data;
    //成功与否标识
    private int code;
    //错误标识
    private String errmsg;

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Result [code=" + code + " ,success=" + success + ", errmsg=" + errmsg +  ", data=" + data + "]";
    }


}
