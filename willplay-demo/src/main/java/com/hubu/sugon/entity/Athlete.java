package com.hubu.sugon.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class Athlete {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private Integer id;

    /**
     * 运动员姓名
     */
    private String name;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM", timezone = "GMT+8")
    private Date birthDate;

    /**
     * 家乡
     */
    private String hometown;

    /**
     * 所属运动队id
     */
    private Integer groupId;
}
