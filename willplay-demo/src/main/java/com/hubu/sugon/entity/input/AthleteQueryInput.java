package com.hubu.sugon.entity.input;

import lombok.Data;

@Data
public class AthleteQueryInput {
    private Integer pageSize;//每一页的容量

    private Integer currentPage;//当前页数
}
