package com.hubu.sugon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hubu.sugon.entity.Athlete;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-12-22
 */
@Mapper
public interface AthleteMapper extends BaseMapper<Athlete> {

}
