package com.hubu.sugon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hubu.sugon.entity.Result;
import com.hubu.sugon.entity.Athlete;
import com.hubu.sugon.entity.input.AthleteQueryInput;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-12-22
 */
public interface IAthleteService extends IService<Athlete> {
    Result getlist(AthleteQueryInput input);
}
