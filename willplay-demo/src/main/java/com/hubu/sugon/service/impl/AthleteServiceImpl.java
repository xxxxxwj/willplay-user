package com.hubu.sugon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;

import com.hubu.sugon.entity.PageInfoResult;
import com.hubu.sugon.entity.Result;
import com.hubu.sugon.entity.RetCodeEnum;
import com.hubu.sugon.service.CommonService;
import com.hubu.sugon.entity.Athlete;
import com.hubu.sugon.entity.input.AthleteQueryInput;
import com.hubu.sugon.mapper.AthleteMapper;
import com.hubu.sugon.service.IAthleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-12-22
 */
@Service
public class AthleteServiceImpl extends ServiceImpl<AthleteMapper, Athlete> implements IAthleteService {
    @Autowired
    AthleteMapper athleteMapper;
    @Autowired
    CommonService commonService;


    @Override
    public Result<PageInfoResult<Athlete>> getlist(AthleteQueryInput input) {
        PageHelper.startPage(input.getCurrentPage(),input.getPageSize());
        List<Athlete> athletes = athleteMapper.selectList(new QueryWrapper<>());
        System.out.println(athletes);
        //PageInfo<Athlete> athletePageInfo = new PageInfo<>(athletes);
        PageInfoResult pageInfoResult = commonService.toPage(athletes);
        return  commonService.initReturnResult(RetCodeEnum.CODE_200,pageInfoResult,true,null);

    }
}
