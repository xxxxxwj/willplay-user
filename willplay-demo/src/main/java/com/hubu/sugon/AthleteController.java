package com.hubu.sugon;


import com.hubu.sugon.entity.PageInfoResult;
import com.hubu.sugon.entity.Result;
import com.hubu.sugon.entity.RetCodeEnum;
import com.hubu.sugon.service.CommonService;
import com.hubu.sugon.entity.Athlete;
import com.hubu.sugon.entity.input.AthleteQueryInput;
import com.hubu.sugon.service.IAthleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-12-22
 */

@CrossOrigin
@RestController
@RequestMapping("/sports/athlete")
public class AthleteController {

    @Autowired
    CommonService commonService;
    @Autowired
    IAthleteService athleteService;

//    @RequestMapping("/info/{id}")
//    public Result<Athlete> info  (@PathVariable("id") Integer id){
//        Athlete athlete = athleteService.getById(id);
//        return BaseResult.success(athlete);
//    }

    @PostMapping("/list")
    public Result<PageInfoResult<Athlete>> getScoreInfo(@RequestBody AthleteQueryInput input){
        Result<PageInfoResult<Athlete>> result;
        try {
            result = athleteService.getlist(input);
        }catch (Exception e){
            e.printStackTrace();
            result = commonService.initReturnResult(RetCodeEnum.CODE_500, false, null);
        }
        return result;

    }
}
