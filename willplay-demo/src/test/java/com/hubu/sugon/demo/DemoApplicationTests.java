package com.hubu.sugon.demo;


import com.hubu.sugon.entity.Result;
import com.hubu.sugon.entity.input.AthleteQueryInput;
import com.hubu.sugon.service.IAthleteService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class DemoApplicationTests {
    @Resource
    IAthleteService athleteService;
    @Test
    public void contextLoads() {
        AthleteQueryInput athleteQueryInput = new AthleteQueryInput();
        athleteQueryInput.setCurrentPage(0);
        athleteQueryInput.setPageSize(3);
        Result getlist = athleteService.getlist(athleteQueryInput);
        System.out.println(getlist);
    }

}
