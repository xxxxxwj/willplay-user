# 总体说明：

1本项目为springcloud基础模块 相关分页工具，返回类（含状态码），通用业务类 包括基础开发所需的各种maven依赖均已在will-common中建立。
开发新的微服务只需点击父项目（willplay）创建模块 ，用普通maven模板创建即可，然后install所给的will-play模块，然后在创建的新的模块中引入即可。具体可参照给出的demo，该demo为创建新模块的例子

```
        <dependency>
            <groupId>com.hubu.sugon</groupId>
            <artifactId>willplay-common</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
```

该demo中还包括了各层的代码如何书写，注意CommonService的运用（进行分页，返回等）。

如对上述过程有疑惑可参考

```
https://www.bilibili.com/video/BV18E411x7eT?p=14
```
2.记得修改每个模块的pom文件的父pom，可参考demo。
# 开发事项

1.在给定的各个微服务的表中进行开发，如有需要加表或者对于表的设计结构字段等有疑问的话，均可和该微服务负责人进行协商后更改。如果需要用到不属于自己微服务的表的需要通过服务调用的形式，请该服务的开发人员提供接口供你调用，不可直接对不属于个人负责的微服务的表进行相关操作。
2.开发时候每个包必须命名为 com.hubu.sugon 不然无法自动注入实例
