FROM java:8
EXPOSE 8081
RUN rm -f /etc/localtime \
&& ln -sv /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
&& echo "Asia/Shanghai" > /etc/timezone
ADD  willplay-user/target/*.jar /willplay-user.jar
CMD ["java","-jar","/willplay-user.jar","--spring.profiles.active=prod"]