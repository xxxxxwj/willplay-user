package com.hubu.sugon.mapper;

import com.hubu.sugon.entity.Coupon;
import com.hubu.sugon.entity.User_Coupon;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface CouponMapper {

    //添加优惠券
    void AddCoupon(Coupon coupon);
    //根据优惠券id查询优惠券金额
    Coupon GetCouponById(String coupon_id);
    //给用户发放优惠券
    void AddUserCoupon(User_Coupon userCoupon);
    //获取所有用户id
    ArrayList GetAllUserId();
    //根据用户id获取有效优惠券关系id
    List<User_Coupon> GetCouponByUserId(String user_id);
    //使用过后，优惠券失效
    void UnValid(String coupon_id);

}
