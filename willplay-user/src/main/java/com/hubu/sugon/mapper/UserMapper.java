package com.hubu.sugon.mapper;

import com.hubu.sugon.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface UserMapper {

    //上传学生证照片
    void UploadStuImg(@Param("id") String id, @Param("stu_img") String stu_img);
    //通过申诉表id获取申诉信息
    Complaint GetComplaintById(String complaint_id);
    //用户创建投诉
    void CreateComplaintsFromUsers(Complaint complaint);
    //获取所有未审核的投诉
    List<Complaint> GetAllComplaintsNotReview();
    //审核投诉
    void CheckComplaint(Complaint complaint);
    //获取所有需要封禁的用户信息
    String GetUserInfoById(String user_id);
    //获取所有未封禁的用户id
    List<Complaint> GetAllUserNotBan();
    //封禁用户
    void BanUser(Complaint complaint);
    //解封用户
    void DeBlock(Complaint complaint);
    //通过id获取申诉表
    Explain GetExplainById(String explain_id);
    //用户创建申诉
    void CreateExplainsFromUsers(Explain explain);
    //获取所有未审核的申诉
    List<Explain> GetAllExplainsNotReview();
    //审核申诉
    void CheckExplain(Explain explain);
    //根据用户id获取所有的投诉
    List<Complaint> GetComplaintsByUserId(String user_id);
    //根据用户id获取所有的申诉
    List<Explain> GetExplainsByUserId(String user_id);
    //审核学生信息
    void CheckStuInfo(Student student);
    //审核商家信息
    void CheckMerchant(Merchant merchant);
    //实名认证
    void CheckCertification(Merchant merchant);
    //发布公告
    void AddNote(Note note);
    //给所有用户发送公告
    void SentNoteToAllUser(User_Note userNote);
    //用户id获取公告id
    List<User_Note> GetNoteIdByUserId(String user_id);
    //公告id获取公告
    Note GetNoteById(String note_id);
    //根据id获取公告id
    User_Note GetNoteIdById(String note_id);
    //更改消息为已读
    void SetIsRead(String note_id);

}