package com.hubu.sugon.controller;

import com.hubu.sugon.entity.*;
import com.hubu.sugon.service.impl.CommonServiceImpl;
import com.hubu.sugon.service.impl.CouponServiceImpl;
import com.hubu.sugon.service.impl.UserServiceImpl;
import com.sun.org.apache.regexp.internal.RE;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/administrator")
@Api(tags = "用户管理接口")
public class UserAdminController {

    @Resource
    private UserServiceImpl userService;

    @Resource
    private CouponServiceImpl couponService;

    @Autowired
    private CommonServiceImpl commonService;

    /**
     * @description: 获取未审核的投诉请求
     * @return List<Complaint> 投诉请求列表
     * **/
    @ApiOperation("获取未审核的投诉请求")
    @RequestMapping(value = "/getComplaintsNotReview",method = RequestMethod.GET)
    public Result GetAllComplaintsNotReview(){
        List<Complaint> list = userService.GetAllComplaintsNotReview();
        PageInfoResult<Complaint> result = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,result,true,"获取数据成功");
    }

    /**
     * @description: 审核投诉请求
     * @param complaint_id 投诉信息id
     * @param is_pass 是否通过 （0：通过，1：不通过）
     * **/
    @ApiOperation("审核投诉请求")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complaint_id",value = "投诉信息id"),
            @ApiImplicitParam(name = "is_pass",value = "是否通过(0:通过，1：不通过)")
    })
    @RequestMapping(value = "/checkComplaints",method = RequestMethod.POST)
    public Result CheckComplaints(String complaint_id,String is_pass){
        userService.CheckComplaint(complaint_id,is_pass);
        if (is_pass.equals("0")){
            return  commonService.initReturnResult(RetCodeEnum.CODE_200,true,"审核通过");
        }else if (is_pass.equals("1")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,false,"审核不通过,投诉失败");
        } else {
             return commonService.initReturnResult(RetCodeEnum.CODE_405,false,"参数错误");
        }
    }

    /**
     * @description: 获取封禁用户信息
     * @return 需封禁用户信息
     * **/
    @ApiOperation("获取未封禁的用户信息")
    @RequestMapping(value = "/userNotBan",method = RequestMethod.GET)
    public Result GetBannedUserInfo(){
        List<Complaint> list = userService.GetAllUserNotBan();
        PageInfoResult page = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,page,true,"获取数据成功");
    }

    /**
     * @description: 封禁用户
     * @param complaint_id 投诉信息id
     * @param banTime 封禁时间
     * @return 相关信息
     * **/
    @ApiOperation("封禁用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complaint_id",value = "投诉信息id"),
            @ApiImplicitParam(name = "banTime",value = "封禁时长"),
    })
    @RequestMapping(value = "/banned",method = RequestMethod.POST)
    public Result Banned(String complaint_id,int banTime){
        Complaint complaint = userService.GetComplaintById(complaint_id);
        String complaints = complaint.getComplainants();
        userService.BannedUser(complaint,complaints,banTime);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"成功封禁"+banTime+"天");
    }

    /**
     * @description 解封用户
     * @param complaint_id 投诉信息id
     * @return 相关信息
     * **/
    @ApiOperation("解封用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complaint_id",value = "投诉信息id")
    })
    @RequestMapping(value = "/deblock",method = RequestMethod.POST)
    public Result DeBlock(String complaint_id){
        Complaint complaint = userService.GetComplaintById(complaint_id);
        String complaints = complaint.getComplainants();
        userService.DeBlock(complaint,complaints);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"解封成功");
    }

    /**
     * @description: 获取所有未审核申诉信息
     * @return 申诉列表
     * **/
    @ApiOperation("获取所有未审核申诉信息")
    @RequestMapping(value = "/getExplainNotReview",method = RequestMethod.GET)
    public Result GetAllExplainsNotReview(){
        List<Explain> list = userService.GetAllExplainsNotReview();
        PageInfoResult<Explain> result = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,result,true,"获取数据成功");
    }

    /**
     * @description: 审核申诉信息
     * @param explain_id 申诉信息id
     * @param is_pass 是否通过 （0：通过，1：不通过）
     * **/
    @ApiOperation("审核申诉信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "explain_id",value = "投诉信息id"),
            @ApiImplicitParam(name = "is_pass",value = "是否通过"),
    })
    @RequestMapping(value = "/checkExplain",method = RequestMethod.POST)
    public Result CheckExplain(String explain_id,String is_pass){
        userService.CheckExplain(explain_id,is_pass);
        if (is_pass.equals("0")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"审核通过");
        }else if (is_pass.equals("1")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,false,"审核不通过,诉失败");
        } else {
            return commonService.initReturnResult(RetCodeEnum.CODE_405,false,"参数错误");
        }
    }

    /**
     * @description: 审核学生信息
     * @param stu_id 学生信息id
     * @param is_valid 是否通过 （0：未认证，1：认证）
     * **/
    @ApiOperation("审核学生信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "stu_id",value = "学生信息id"),
            @ApiImplicitParam(name = "is_valid",value = "是否通过"),
    })
    @RequestMapping(value = "/StudentCertification",method = RequestMethod.POST)
    public Result CheckStuInfo(String stu_id,String is_valid){
        userService.CheckStuInfo(stu_id,is_valid);
        if (is_valid.equals("1")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"认证通过");
        }else if (is_valid.equals("0")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,false,"认证不通过");
        } else {
            return commonService.initReturnResult(RetCodeEnum.CODE_405,false,"参数错误");
        }
    }

    /**
     * @description: 审核商家权限信息
     * @param merchant_id 商家权限信息id
     * @param is_normal 是否通过 （1：未认证，0：认证）
     * **/
    @ApiOperation("审核商家认证信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "merchant_id",value = "商家权限信息id"),
            @ApiImplicitParam(name = "is_normal",value = "是否通过"),
    })
    @RequestMapping(value = "/MerchantAuthentication",method = RequestMethod.POST)
    public Result CheckMerchant(String merchant_id,String is_normal){
        userService.CheckMerchant(merchant_id,is_normal);
        if (is_normal.equals("0")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"认证通过");
        }else if (is_normal.equals("1")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,false,"认证不通过");
        } else {
            return commonService.initReturnResult(RetCodeEnum.CODE_405,false,"参数错误");
        }
    }

    /**
     * @description 审核商家实名信息
     * @param merchant_id 商家权限信息id
     * @param is_certification 是否实名
     * @return 相关信息
     * **/
    @ApiOperation("审核商家实名信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "merchant_id",value = "商家权限信息id"),
            @ApiImplicitParam(name = "is_certification",value = "是否实名"),
    })
    @RequestMapping(value = "/authentication",method = RequestMethod.POST)
    public Result CheckCertification(String merchant_id,String is_certification){
        userService.CheckCertification(merchant_id,is_certification);
        if (is_certification.equals("1")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"认证通过");
        }else if (is_certification.equals("0")){
            return commonService.initReturnResult(RetCodeEnum.CODE_200,false,"认证不通过");
        } else {
            return commonService.initReturnResult(RetCodeEnum.CODE_405,false,"参数错误");
        }
    }

    /**
     * @description 添加优惠券
     * @param coupon_name 优惠券名称
     * @param sum 优惠券金额
     * @param description 优惠券描述
     * @param activity 使用活动名称
     * @param merchant_id 使用商家id
     * **/
    @ApiOperation("添加优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coupon_name",value = "优惠券名称"),
            @ApiImplicitParam(name = "sum",value = "优惠券金额"),
            @ApiImplicitParam(name = "description",value = "优惠券描述"),
            @ApiImplicitParam(name = "activity",value = "使用活动名称"),
            @ApiImplicitParam(name = "merchant_id",value = "使用商家id")
    })
    @RequestMapping(value = "/addCoupon",method = RequestMethod.PUT)
    public Result AddCoupon(String coupon_name, BigDecimal sum, String description, String activity, String merchant_id){
        couponService.AddCoupon(coupon_name,sum,description,activity,merchant_id);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"添加成功！");
    }

    /**
     * @description 给个别用户发放优惠券
     * @param user_id 用户id
     * @param coupon_id 优惠券id
     * @param validTime 有效时间
     * **/
    @ApiOperation("给个别用户发放优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user_id",value = "用户id"),
            @ApiImplicitParam(name = "coupon_id",value = "优惠券id"),
            @ApiImplicitParam(name = "validTime",value = "有效时间")
    })
    @RequestMapping(value = "/sentCoupon",method = RequestMethod.PUT)
    public Result SentCouponToUser(String user_id, String coupon_id, int validTime){
        couponService.AddUserCoupon(user_id,coupon_id,validTime);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"发放优惠券成功！");
    }

    /**
     * @description 给所有用户发放优惠券
     *
     * **/
    @ApiOperation("给所有用户发放优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coupon_id",value = "优惠券id"),
            @ApiImplicitParam(name = "validTime",value = "有效时间")
    })
    @RequestMapping(value = "/sentCpntoAllUser",method = RequestMethod.PUT)
    public Result SentCouponToAllUser(String coupon_id,int validTime){
        couponService.SentUserCoupon(coupon_id,validTime);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"发放优惠券成功！");
    }

    /**
     * @description 给所有用户发id
     * @param note 公告内容
     * **/
    @ApiOperation("给所有用户发送公告")
    @ApiImplicitParam(name = "note",value = "公告内容")
    @RequestMapping(value = "/sentNote",method = RequestMethod.PUT)
    public Result SentNoteToAllUser(String note){
        userService.SentNoteToAllUser(note);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"发送公告成功!");
    }

}
