package com.hubu.sugon.controller;

import com.hubu.sugon.entity.*;
import com.hubu.sugon.service.impl.CommonServiceImpl;
import com.hubu.sugon.service.impl.CouponServiceImpl;
import com.hubu.sugon.service.impl.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
@Api(tags = "用户接口")
public class UserController {

    @Autowired
    private CouponServiceImpl couponService;

    @Resource
    private UserServiceImpl userService;

    @Resource
    private CommonServiceImpl commonService;

    /**
     * @description 上传学生证照片
     * @param id 学生id
     * @param stu_img 学生证照片
     * **/
    @ApiOperation("上传学生证照片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "学生id"),
            @ApiImplicitParam(name = "stu_img",value = "学生证照片")
    })
    @RequestMapping(value = "/uploadStuImg",method = RequestMethod.POST)
    public Result UploadStuImg(String id,String stu_img){
        userService.UploadStuImg(id,stu_img);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"上传成功！");
    }

    /**
     * @description: 用户发起投诉
     * @param complainants 被投诉者id
     * @param complaints_img 投诉图片证据
     * @param complaints_reason 投诉原因
     * @param user_id 发起投诉的用户id
     * @return 返回相关结果
     * **/
    @ApiOperation("用户发起投诉")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainants",value = "被投诉者id"),
            @ApiImplicitParam(name = "complaints_reason",value = "投诉原因"),
            @ApiImplicitParam(name = "complaints_img",value = "投诉图片证据"),
            @ApiImplicitParam(name = "user_id",value = "投诉者id")
    })
    @RequestMapping(value = "/createComplaint/{user_id}",method = RequestMethod.PUT)
    public Result UpdateComplaintsFromUser( String complainants, String complaints_reason, String complaints_img, @PathVariable("user_id") String user_id){
        userService.UpdateComplaintsFromUser(complainants,complaints_reason,complaints_img,user_id);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"投诉成功");
    }

    /**
     * @description: 用户发起申诉
     * @param complaint_id 投诉表id
     * @param user_id 发起申诉的用户id
     * @param explain_img 申诉图片证据
     * @param explain_reason 申诉原因
     * @return 相关信息
     * **/
    @ApiOperation("用户发起申诉")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complaint_id",value = "申诉对象"),
            @ApiImplicitParam(name = "user_id",value = "申诉者id"),
            @ApiImplicitParam(name = "explain_reason",value = "申诉原因"),
            @ApiImplicitParam(name = "explain_img",value = "申诉图片证据")
    })
    @RequestMapping(value = "/createExplain/{user_id}",method = RequestMethod.PUT)
    public Result UpdateExplainFromUser(String complaint_id,@PathVariable("user_id") String user_id,String explain_reason,String explain_img){
        userService.CreateExplainFromUser(explain_img,explain_reason,user_id,complaint_id);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"发起申诉成功");
    }

    /**
     * @description 获取该用户的所有投诉信息
     * @param user_id 用户id
     * @return 数据列表
     * **/
    @ApiOperation("获取该用户的所有投诉信息")
    @ApiImplicitParam(name = "user_id",value = "用户id")
    @RequestMapping(value = "/getAllcmp/{user_id}",method = RequestMethod.GET)
    public Result GetAllComplaints(@PathVariable String user_id){
        List<Complaint> list = userService.GetAllComplaints(user_id);
        PageInfoResult page = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,page,true,"获取成功！");
    }

    /**
     * @description 获取该用户的所有申诉信息
     * @param user_id 用户id
     * @return 数据列表
     * **/
    @ApiOperation("获取该用户的所有申诉信息")
    @ApiImplicitParam(name = "user_id",value = "用户id")
    @RequestMapping(value = "/getAllExp/{user_id}",method = RequestMethod.GET)
    public Result GetAllExplains(@PathVariable String user_id){
        List<Explain> list = userService.GetAllExplains(user_id);
        PageInfoResult page = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,page,true,"获取成功！");
    }

    /**
     * @description 根据优惠券id获取优惠券信息
     * @param coupon_id 优惠券id
     * @return 相关信息
     * **/
    @ApiOperation("根据优惠券id获取优惠券信息")
    @ApiImplicitParam(name = "coupon_id",value = "优惠券id")
    @RequestMapping(value = "/getCoupon",method = RequestMethod.GET)
    public Result GetSumById(String coupon_id){
        Coupon coupon = couponService.GetCouponById(coupon_id);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,coupon,true,"该优惠券金额："+coupon.getSum());
    }

    /**
     * @description 获取该用户所有有效优惠券
     * @param user_id 用户id
     * @return  相关信息
     * **/
    @ApiOperation("获取该用户所有优惠券id")
    @ApiImplicitParam(name = "user_id",value = "用户id")
    @RequestMapping(value = "/getAllCoupon/{user_id}",method = RequestMethod.GET)
    public Result GetAllCouponByUserId(@PathVariable("user_id") String user_id){
        ArrayList list = couponService.GetCouponInfoByUserId(user_id);
        PageInfoResult page = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,page,true,"获取成功！");
    }

    /**
     * @description 已使用优惠券失效
     * @param coupon_id 用户拥有优惠券id
     * **/
    @ApiOperation("已使用优惠券失效")
    @ApiImplicitParam(name = "coupon_id",value = "用户拥有优惠券id")
    @RequestMapping(value = "/unValid",method = RequestMethod.POST)
    public Result UnValid(String coupon_id){
        couponService.UnValid(coupon_id);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,true,"优惠券已失效");
    }

    /**
     * @description 根据用户id获取所有公告
     * @param user_id 用户id
     * **/
    @ApiOperation("根据用户id获取所有公告")
    @ApiImplicitParam(name = "user_id",value = "用户id")
    @RequestMapping(value = "/getAllnote",method = RequestMethod.GET)
    public Result GetAllNoteByUserId(String user_id){
        ArrayList list = userService.GetAllNoteByUserId(user_id);
        PageInfoResult page = commonService.toPage(list);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,page,true,"获取成功!");
    }

    /**
     * @description 根据公告id获取公告
     * @param note_id
     * **/
    @ApiOperation("根据公告id获取公告")
    @ApiImplicitParam(name = "note_id",value = "公告id")
    @RequestMapping(value = "/getNote",method = RequestMethod.GET)
    public Result GetNoteByNoteId(String note_id){
        Note note = userService.GetNoteByNoteId(note_id);
        return commonService.initReturnResult(RetCodeEnum.CODE_200,note,true,"获取成功!");
    }

}
