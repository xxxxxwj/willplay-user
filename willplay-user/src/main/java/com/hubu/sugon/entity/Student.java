package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@ApiModel(value = "学生表")
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @ApiModelProperty
    private String id;
    @ApiModelProperty
    private Date create_time;
    @ApiModelProperty
    private Date update_time;
    @ApiModelProperty
    private Date delete_time;
    @ApiModelProperty
    private String stu_num;
    @ApiModelProperty
    private String name;
    @ApiModelProperty
    private String stu_img;
    @ApiModelProperty
    private String is_valid;

}
