package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@Data
@ApiModel(value = "投诉表")
@NoArgsConstructor
@AllArgsConstructor
public class Complaint {

    @ApiModelProperty
    private String id;
    @ApiModelProperty
    private Date create_time;
    @ApiModelProperty
    private Date update_time;
    @ApiModelProperty(name = "投诉者id")
    private String create_by;
    @ApiModelProperty(name = "被投诉者id")
    private String complainants;
    @ApiModelProperty(name = "被投诉者名称")
    private String complainants_name;
    @ApiModelProperty
    private String complaints_reason;
    @ApiModelProperty
    private String complaints_img;
    @ApiModelProperty
    private String is_pass;
    @ApiModelProperty
    private Date ban_time;

}
