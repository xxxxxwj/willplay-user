package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@ApiModel(value = "申诉表")
@NoArgsConstructor
@AllArgsConstructor
public class Explain {

    @ApiModelProperty
    private String id;
    @ApiModelProperty
    private Date create_time;
    @ApiModelProperty
    private Date update_time;
    @ApiModelProperty
    private String create_by;
    @ApiModelProperty
    private String complaint_id;
    @ApiModelProperty
    private String explain_reason;
    @ApiModelProperty
    private String explain_img;
    @ApiModelProperty
    private String is_pass;

}
