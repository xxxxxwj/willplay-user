package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("用户公告表")
public class User_Note {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("用户id")
    private String user_id;
    @ApiModelProperty("公告id")
    private String note_id;
    @ApiModelProperty("是否读过")
    private String is_read;
    @ApiModelProperty("创建时间")
    private Date create_time;
    @ApiModelProperty("更新时间")
    private Date update_time;

}
