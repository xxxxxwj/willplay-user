package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("商家表")
public class Merchant {

    @ApiModelProperty("商家信息id")
    private String id;
    @ApiModelProperty("更新时间")
    private Date update_time;
    @ApiModelProperty("是否正常")
    private String is_normal;
    @ApiModelProperty("是否实名认证")
    private String is_certification;

}
