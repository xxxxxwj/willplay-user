package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("公告表")
public class Note {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("公告信息")
    private String note;
    @ApiModelProperty("创建时间")
    private Date create_time;
    @ApiModelProperty("更新时间")
    private Date update_time;

}
