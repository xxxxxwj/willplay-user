package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("用户-优惠券关系表")
public class User_Coupon {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("用户id")
    private String user_id;
    @ApiModelProperty("优惠券id")
    private String coupon_id;
    @ApiModelProperty("是否有效")
    private String is_valid;
    @ApiModelProperty("开始时间")
    private Date start_time;
    @ApiModelProperty("结束时间")
    private Date end_time;

}
