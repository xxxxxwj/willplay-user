package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : xwj
 * @date :
 * @description:
 **/
@Data
@ApiModel("用户表")
public class User {

    @ApiModelProperty("用户id")
    private String id;
    @ApiModelProperty("用户名称")
    private String user_name;

}
