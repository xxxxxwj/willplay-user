package com.hubu.sugon.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("优惠券表")
public class Coupon {

    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("金额")
    private BigDecimal sum;
    @ApiModelProperty("名称")
    private String coupon_name;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("创建时间")
    private Date create_time;
    @ApiModelProperty("更新时间")
    private Date update_time;
    @ApiModelProperty("使用的活动名称")
    private String activity;
    @ApiModelProperty("使用的商家id")
    private String merchant_id;

}
