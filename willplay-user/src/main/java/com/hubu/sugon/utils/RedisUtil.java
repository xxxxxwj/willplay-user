package com.hubu.sugon.utils;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void BlockRedis(String complainants,long BanTime){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("is_normal", JSON.toJSONString(1));
        stringRedisTemplate.opsForHash().putAll(complainants,map);
        stringRedisTemplate.expire(complainants,BanTime, TimeUnit.DAYS);
    }

    public void DeBlockRedis(String complainants){
        stringRedisTemplate.delete(complainants);
    }

}
