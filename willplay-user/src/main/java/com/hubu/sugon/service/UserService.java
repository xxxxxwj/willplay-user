package com.hubu.sugon.service;

import com.hubu.sugon.entity.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public interface UserService {

    void UploadStuImg(String id,String stu_img);
    void UpdateComplaintsFromUser(String complainants,String complaints_reason,String complaints_img,String user_id);
    List<Complaint> GetAllComplaintsNotReview();
    Complaint GetComplaintById(String complaint_id);
    void CheckComplaint(String complaint_id,String is_pass);
    List<Complaint> GetAllUserNotBan();
    void BannedUser(Complaint complaint, String complaints, int BanTimed);
    void DeBlock(Complaint complaint,String complaints);
    void CreateExplainFromUser(String explain_img,String explain_reason,String user_id,String complaint_id);
    List<Explain> GetAllExplainsNotReview();
    void CheckExplain(String explain_id,String is_pass);
    List<Complaint> GetAllComplaints(String user_id);
    List<Explain> GetAllExplains(String user_id);
    void CheckStuInfo(String stu_id,String is_valid);
    void CheckMerchant(String merchant_id,String is_normal);
    void CheckCertification(String merchant_id,String is_certification);
    void SentNoteToAllUser(String note);
    ArrayList GetAllNoteByUserId(String user_id);
    Note GetNoteByNoteId(String note_id);

}
