package com.hubu.sugon.service;

import com.hubu.sugon.entity.Coupon;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;

@Service
public interface CouponService {

    void AddCoupon(String coupon_name, BigDecimal sum, String description,
                   String activity, String merchant_id);
    Coupon GetCouponById(String coupon_id);
    void AddUserCoupon(String user_id, String coupon_id,int validTime);
    void SentUserCoupon(String coupon_id,int validTime);
    ArrayList GetCouponInfoByUserId(String user_id);
    void UnValid(String coupon_id);

}
