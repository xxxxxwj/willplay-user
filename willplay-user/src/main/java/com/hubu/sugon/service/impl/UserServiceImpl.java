package com.hubu.sugon.service.impl;

import com.hubu.sugon.mapper.CouponMapper;
import com.hubu.sugon.utils.RedisUtil;
import com.hubu.sugon.utils.SnowflakeId;
import com.hubu.sugon.entity.*;
import com.hubu.sugon.mapper.UserMapper;
import com.hubu.sugon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SnowflakeId snowflakeId;

    @Resource
    private UserMapper userMapper;

    @Resource
    private CouponMapper couponMapper;

    @Override
    public void UploadStuImg(String id,String stu_img){
        userMapper.UploadStuImg(id,stu_img);
    }

    @Override
    public void UpdateComplaintsFromUser(String complainants,String complaints_reason,String complaints_img,String user_id) {
        String user_name = userMapper.GetUserInfoById(complainants);
        Complaint complaint = new Complaint();
        Date date = new Date();
        complaint.setCreate_time(date);
        complaint.setCreate_by(user_id);
        complaint.setComplainants(complainants);
        complaint.setComplainants_name(user_name);
        complaint.setComplaints_img(complaints_img);
        complaint.setComplaints_reason(complaints_reason);
        Long complaint_id = snowflakeId.nextId();
        complaint.setId(complaint_id.toString());
        userMapper.CreateComplaintsFromUsers(complaint);
    }

    @Override
    public List<Complaint> GetAllComplaintsNotReview() {
        List<Complaint> list = userMapper.GetAllComplaintsNotReview();
        return list;
    }

    @Override
    public Complaint GetComplaintById(String complaint_id){
        return userMapper.GetComplaintById(complaint_id);
    }

    @Override
    public void CheckComplaint(String complaint_id ,String is_pass) {
        Complaint complaint = userMapper.GetComplaintById(complaint_id);
        complaint.setUpdate_time(new Date());
        complaint.setIs_pass(is_pass);
        userMapper.CheckComplaint(complaint);
    }

    @Override
    public List<Complaint> GetAllUserNotBan(){
        return userMapper.GetAllUserNotBan();
    }

    @Override
    public void BannedUser(Complaint complaint, String complaints, int BanTime) {
        Calendar cal = Calendar.getInstance();
        Date date = new Date();
        cal.setTime(date);
        cal.add(Calendar.DATE,BanTime);
        complaint.setBan_time(cal.getTime());
        complaint.setUpdate_time(date);
        redisUtil.BlockRedis(complaints,BanTime);
        userMapper.BanUser(complaint);
    }

    @Override
    public void DeBlock(Complaint complaint,String complaints){
        Date date = new Date();
        complaint.setUpdate_time(date);
        complaint.setBan_time(date);
        redisUtil.DeBlockRedis(complaints);
        userMapper.DeBlock(complaint);
    }

    @Override
    public void CreateExplainFromUser(String explain_img, String explain_reason, String user_id, String complaint_id) {
        Explain explain = new Explain();
        explain.setCreate_time(new Date());
        explain.setCreate_by(user_id);
        explain.setComplaint_id(complaint_id);
        Long explain_id = snowflakeId.nextId();
        explain.setId(explain_id.toString());
        explain.setExplain_img(explain_img);
        explain.setExplain_reason(explain_reason);
        userMapper.CreateExplainsFromUsers(explain);
    }

    @Override
    public List<Explain> GetAllExplainsNotReview() {
        List<Explain> list = userMapper.GetAllExplainsNotReview();
        return list;
    }

    @Override
    public void CheckExplain(String explain_id, String is_pass) {
        Explain explain = userMapper.GetExplainById(explain_id);
        explain.setUpdate_time(new Date());
        explain.setIs_pass(is_pass);
        userMapper.CheckExplain(explain);
    }

    @Override
    public List<Complaint> GetAllComplaints(String user_id){
        return userMapper.GetComplaintsByUserId(user_id);
    }

    @Override
    public List<Explain> GetAllExplains(String user_id){
        return userMapper.GetExplainsByUserId(user_id);
    }

    @Override
    public void CheckStuInfo(String stu_id,String is_valid){
        Student student = new Student();
        student.setId(stu_id);
        student.setIs_valid(is_valid);
        Date date = new Date();
        student.setUpdate_time(date);
        userMapper.CheckStuInfo(student);
    }

    @Override
    public void CheckMerchant(String merchant_id,String is_normal){
        Merchant merchant = new Merchant();
        merchant.setIs_normal(is_normal);
        merchant.setId(merchant_id);
        Date date = new Date();
        merchant.setUpdate_time(date);
        userMapper.CheckMerchant(merchant);
    }

    @Override
    public void CheckCertification(String merchant_id,String is_certification){
        Merchant merchant = new Merchant();
        merchant.setIs_certification(is_certification);
        merchant.setId(merchant_id);
        Date date = new Date();
        merchant.setUpdate_time(date);
        userMapper.CheckCertification(merchant);
    }

    @Override
    public void SentNoteToAllUser(String note) {
        Note nt = new Note();
        Long id = snowflakeId.getNextId();
        nt.setId(id.toString());
        nt.setNote(note);
        nt.setCreate_time(new Date());
        userMapper.AddNote(nt);
        ArrayList list = couponMapper.GetAllUserId();
        for (Object user_id : list){
            User_Note userNote = new User_Note();
            Long n_id = snowflakeId.getNextId();
            userNote.setId(n_id.toString());
            userNote.setCreate_time(new Date());
            userNote.setNote_id(id.toString());
            userNote.setUser_id(user_id.toString());
            userMapper.SentNoteToAllUser(userNote);
        }
    }

    @Override
    public ArrayList GetAllNoteByUserId(String user_id) {
        List<User_Note> list = userMapper.GetNoteIdByUserId(user_id);
        ArrayList result = new ArrayList();
        for (User_Note userNote:list) {
            Note note = userMapper.GetNoteById(userNote.getNote_id());
            Map map = new HashMap();
            map.put("NoteContent",note.getNote());
            map.put("NoteId",userNote.getId());
            map.put("CreateTime",note.getCreate_time());
            map.put("IsRead",userNote.getIs_read());
            result.add(map);
        }
        return result;
    }

    @Override
    public Note GetNoteByNoteId(String note_id) {
        User_Note UserNote = userMapper.GetNoteIdById(note_id);
        Note note = userMapper.GetNoteById(UserNote.getNote_id());
        userMapper.SetIsRead(note_id);
        return note;
    }


}
