package com.hubu.sugon.service.impl;

import com.hubu.sugon.entity.Coupon;
import com.hubu.sugon.entity.User_Coupon;
import com.hubu.sugon.mapper.CouponMapper;
import com.hubu.sugon.service.CouponService;
import com.hubu.sugon.utils.SnowflakeId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class CouponServiceImpl implements CouponService {

    @Resource
    private CouponMapper couponMapper;

    @Override
    public void AddCoupon(String coupon_name, BigDecimal sum, String description, String activity, String merchant_id) {
        Coupon coupon = new Coupon();
        Long id = SnowflakeId.nextId();
        coupon.setId(id.toString());
        coupon.setActivity(activity);
        coupon.setCoupon_name(coupon_name);
        coupon.setCreate_time(new Date());
        coupon.setDescription(description);
        coupon.setMerchant_id(merchant_id);
        coupon.setSum(sum);
        couponMapper.AddCoupon(coupon);
    }

    @Override
    public Coupon GetCouponById(String coupon_id) {
        return couponMapper.GetCouponById(coupon_id);
    }

    @Override
    public void AddUserCoupon(String user_id, String coupon_id,int validTime) {
        User_Coupon userCoupon = new User_Coupon();
        userCoupon.setCoupon_id(coupon_id);
        Long id = SnowflakeId.nextId();
        userCoupon.setId(id.toString());
        Calendar cal = Calendar.getInstance();
        Date date = new Date();
        cal.setTime(date);
        cal.add(Calendar.DATE,validTime);
        userCoupon.setEnd_time(cal.getTime());
        userCoupon.setStart_time(date);
        userCoupon.setUser_id(user_id);
        couponMapper.AddUserCoupon(userCoupon);
    }

    @Override
    public void SentUserCoupon(String coupon_id, int validTime) {
        ArrayList list = couponMapper.GetAllUserId();
        for(Object user_id : list){
            AddUserCoupon((String) user_id,coupon_id,validTime);
        }
    }

    @Override
    public ArrayList GetCouponInfoByUserId(String user_id) {
        ArrayList list = new ArrayList();
        List<User_Coupon> userCoupons = couponMapper.GetCouponByUserId(user_id);
        try {
            for (User_Coupon userCoupon : userCoupons) {
                String coupon_id = userCoupon.getCoupon_id();
                Coupon coupon = couponMapper.GetCouponById(coupon_id);
                if (userCoupon.getEnd_time().getTime() < new Date().getTime()){
                    UnValid(userCoupon.getId());
                }else {
                    Map map = new HashMap();
                    map.put("coupon_id", userCoupon.getId());
                    map.put("coupon_name", coupon.getCoupon_name());
                    map.put("coupon_sum", coupon.getSum().toString());
                    map.put("description", coupon.getDescription());
                    map.put("activity_name", coupon.getActivity());
                    map.put("merchant", coupon.getMerchant_id());
                    map.put("start_time", userCoupon.getStart_time().toString());
                    map.put("end_time", userCoupon.getEnd_time().toString());
                    list.add(map);
                }
            }
        }catch (Exception e){
            return list;
        }

        return list;
    }

    @Override
    public void UnValid(String coupon_id) {
        couponMapper.UnValid(coupon_id);
    }

}
