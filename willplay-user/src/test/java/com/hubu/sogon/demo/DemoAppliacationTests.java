package com.hubu.sogon.demo;

import com.alibaba.fastjson.JSON;
import com.hubu.sugon.mapper.CouponMapper;
import com.hubu.sugon.utils.SnowflakeId;
import com.hubu.sugon.service.impl.UserServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author : xwj
 * @date :
 * @description:
 **/
public class DemoAppliacationTests {

    @Resource
    private CouponMapper couponMapper;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Test
    public void main1() {
        for (int i = 0;i<100;i++){
            Long id = SnowflakeId.nextId();
            System.out.println(id);
        }
    }

    @Test
    public void setRedis(){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("is_normal", JSON.toJSONString(1));
        stringRedisTemplate.opsForHash().putAll("45465456", map);
        stringRedisTemplate.expire("1",3,TimeUnit.DAYS);
    }

    @Test
    public void main(){
        ArrayList l = couponMapper.GetAllUserId();
        System.out.println(l.toString());
    }

//    @Test
//    public void GetCoupon(){
//        String user_id =
//        ArrayList list_coupon_id = couponMapper.GetCouponByUserId(user_id);
//    }

}
